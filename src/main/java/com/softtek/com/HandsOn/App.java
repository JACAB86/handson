package com.softtek.com.HandsOn;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
/**
 * Hello world!
 *
 */
public class App 
{
	
	
	
    public static void main( String[] args )
    {
    	Device dev = new Device();
    	DataBaseAccess conectar = new DataBaseAccess();
    	String SQL_SELECT = "Select * from Device";
        Connection conn=null;
        DataBaseAccess conexion = new DataBaseAccess();
        conn = conexion.Conectar();
        
    	if(conn!=null) {
            System.out.println("Connected to the database!");
            
            PreparedStatement preparedStatement;
			try {
				preparedStatement = conn.prepareStatement(SQL_SELECT);
                ResultSet resultSet = preparedStatement.executeQuery();
				while (resultSet.next()) {
				                	
				              long id = resultSet.getLong("DEVICEID");
				              String name = resultSet.getString("NAME");
				              String description = resultSet.getString("DESCRIPTION");
				              long manufacturer = resultSet.getLong("MANUFACTURERID");
				              long colorId = resultSet.getLong("COLORID");
				              String comments = resultSet.getString("COMMENTS");
				                    
				                    //Using select  * from device,get all devices in a list
				              MySupplier supplier = (i, n, d, m, ci, cm) -> new Device(i, n, d, m, ci, cm);
				              dev.Devices.add(new Device(id,name,description,manufacturer,colorId,comments));	
				}
			}catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}else {
    		System.out.println("Conexion no establecida");
    	}
 
    	dev.inAListDeviceName();
    	dev.KeepLaptop();
    	dev.GetManufacturerCount();
    	dev.GetColorIdStream();
    	dev.MapClassMap();
    	dev.Imprimir();
    }
}
