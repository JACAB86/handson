package com.softtek.com.HandsOn;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Device {

	long id;
	String name;
	String description;
	long manufacturer;
	long colorId;
	long noOfOccurs;
	String comments;
	ArrayList<Device> Devices = new ArrayList<Device>();
	List<String> Equipos;
	List<String> ListaLaptops;
	ArrayList<Device> ListaDevicesColor = new ArrayList<Device>();
	List<Device> newList;
	Map<Long, Device> newList2;

	
	public Device(long id, String name, String description, long manufacturer, long colorId, String comments) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.manufacturer = manufacturer;
		this.colorId = colorId;
		this.comments = comments;
	}
	
	public Device() {}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(long manufacturer) {
		this.manufacturer = manufacturer;
	}

	public long getColorId() {
		return colorId;
	}

	public void setColorId(long colorId) {
		this.colorId = colorId;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
/*
	public void objectsToList(ResultSet resulSet)  {
		while (resultSet.next()) {
        	
        	long id = resultSet.getLong("DEVICEID");
            String name = resultSet.getString("NAME");
            String description = resultSet.getString("DESCRIPTION");
            long manufacturer = resultSet.getLong("MANUFACTURERID");
            long colorId = resultSet.getLong("COLORID");
            String comments = resultSet.getString("COMMENTS");
            
            //Using select  * from device,get all devices in a list
            MySupplier supplier = (i, n, d, m, ci, cm) -> new Device(i, n, d, m, ci, cm);
            Devices.add(new Device(id,name,description,manufacturer,colorId,comments));
        	
            Map<Long, String> mapa = new HashMap<Long,String>();
		}
	}
*/
	public void Espejo(ArrayList<Device>  Devicess) {
		Devices = Devicess;
	}

	public void inAListDeviceName() {
		Equipos = Devices.stream().map(x -> x.getName()).collect(Collectors.toList());
    	//Lista con todos los objetos
	}
	
	public void KeepLaptop() {
		ListaLaptops = Equipos.stream().filter(x -> "Laptop".equals(x)).collect(Collectors.toList());
		//Lista con solo laptops
	}
	
	public void GetManufacturerCount() {
		noOfOccurs =Devices.stream().map(x -> x.getManufacturer()).filter(x -> x == 3).count();
	//	Collections.frequency(animals, "bat");
       
        //Ocurrencias manufacturerId ==3
	}
	
	public void GetColorIdStream() {

        newList = Devices.stream().filter(n -> n.getColorId() == 1).collect(Collectors.toList());
		 //ListaDevicesColor.stream().filter(x-> x.getColorId() == 1).collect(Collectors.toList());
		//	ListaDevicesColor = Devices.stream().map(Devices::Device).filter(x-> x.getColorId() == 1).collect(Collectors.toList());
	//	ListaDevicesColor.stream().collect(Collectors.groupingBy(Devices.stream().filter(x -> x.getColorId()==3)));
	}
	
	public void MapClassMap() {
		newList2 = Devices.stream().collect(Collectors.toMap(Device::getId, device-> device));
	}
	
	public void Imprimir() {
		System.out.println("Objetos Devices:");
		Devices.forEach(System.out::println);//Query results mappet to object
		
		System.out.println("--------------------------");
		
		System.out.println("Equipos en List<String>");
        System.out.println(Equipos); //[A, B, C, D] Imprime Array
        
        System.out.println("--------------------------");
        
        System.out.println("Lista de Laptops");//Imprime Lista de String con Laptops
        System.out.println(ListaLaptops);
        
        System.out.println("--------------------------");
        
        //Coincidencias con ManufacturerID
        System.out.println("Elementos con ManufacturerId(3): " + noOfOccurs);
        
        System.out.println("--------------------------");
        
        System.out.println("ColorID --> 1"); //Objetos con ColorId = 1 
        System.out.println(newList);
        
        System.out.println("--------------------------");
        
        System.out.println("Mapa de objetos:");//Map of List
        System.out.println(newList2);
        
	}
	
	@Override
	public String toString() {
		return "Device [DeviceId= " + id + ", "
					   + "name= " + name + ", "
					   + "description= " + description + ", " 
					   + "manufacturerId= "+ manufacturer + ", "
					   + "colorId= " + colorId + ", " 
					   + "comments=" + comments + 
//					   + "DeviceId=" + getId() + ", "
//					   + "getName()=" + getName() + ", "
//					   + "getDescription()=" + getDescription()+ ", " 
//					   + "getManufacturerId()=" + getManufacturer() + ", "
//					   + "getColorId()=" + getColorId() + ", "
//					   +" getComments()="+ getComments()+
//					   + "getClass()=" + getClass() + ", " 
//					   + "hashCode()=" + hashCode() + ", " 
//					   + "toString()=" + super.toString() + "]";
					   "]";
	}

	
}
