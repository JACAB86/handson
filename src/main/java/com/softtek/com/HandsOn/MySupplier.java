package com.softtek.com.HandsOn;


@FunctionalInterface

public interface MySupplier<C extends Device> {
	public Device get(long id,String name, String description, long manufacturer, long colorId, String comments);
}
